import React, { useEffect, useState, createContext } from 'react';
import appConfig from '../config';
import { getSecretWordsFromObject } from '../services/service';

export const WordAsterixContext = createContext();

const WordAsterixProvider = ({children, secretWords}) => {
  const showError = (secretWords.error) ? true : false;
  let guessAttempts = appConfig.attempts;

  const [guessedWord, setGuessedWord] = useState('');
  const [secretWordsArray, setSecretWordsArray] = useState(showError ? []: getSecretWordsFromObject(secretWords.secret));
  const [secretWord, setSecretWord] = useState('');
  const [validation, setValidation] = useState(false);
  const [isCorrect, setIsCorrect] = useState(false);
  const [attempts, setAttempts] = useState(false);
  const [attemptsCount, setAttemptsCount] = useState(0);
  const [gameOver, setGameOver] = useState(false);
  const secretWordsCollection = secretWords.secret;

  useEffect(() => {
    handleSecretWordArray();
    pickSecretWord();
  }, []);

  const handleSecretWordArray = () => {
    if(showError) return;
    setSecretWordsArray(secretWords.secret);
  };

  const pickSecretWord = () => {
    return setSecretWord(secretWordsArray[0].word);
  };

  const removedSuccessfulWordGuessed = () =>  {
    if(secretWordsArray.length > -1 ) {
      secretWordsArray.shift();
      setSecretWordsArray(secretWordsArray);
      
      if(secretWordsArray.length === 0) {
        setGameOver(true);
      }
    }
  };

  const storeGuess = (usersGuess) => {
    if(usersGuess === '') {
      setValidation(true);
      return;
    } else if(usersGuess === secretWord) {
      setIsCorrect(true);
      removedSuccessfulWordGuessed();
    } else {
      setValidation(false);
      handleAttempts();
    }

    setValidation(false);
    setGuessedWord(usersGuess);
  };

  const playAgain = () => {
    setAttempts(false);
    setAttemptsCount(0);
    setIsCorrect(false);
    setGuessedWord('');
    pickSecretWord();
  };

  const handleAttempts = () => {
    let countAttempts;
    countAttempts = attemptsCount;
    setAttemptsCount(countAttempts+=1);

    if( (countAttempts < guessAttempts) ) {
      setAttempts(false);
    } else {
      setAttempts(true);
    }
  };

  const providerProps = {
    showError,
    secretWord,
    storeGuess,
    guessedWord,
    validation,
    isCorrect,
    attempts,
    attemptsCount,
    playAgain,
    gameOver,
    secretWordsCollection
  };

  return (
    <WordAsterixContext.Provider value={providerProps}>
      { children }
    </WordAsterixContext.Provider>
  )
};


export default WordAsterixProvider;
