const appConfig = {
  attempts: 10,
  api: 'http://localhost:3333/get-secret-word'
};

export default appConfig;
