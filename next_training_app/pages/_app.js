import '../styles/globals.css'
import Layout from '../components/layout';
import WordAsterixProvider from '../context/asterixContext';

function MyApp({ Component, pageProps }) {
  return (
    <Layout>
      <WordAsterixProvider  {...pageProps}>
        <Component {...pageProps} />
      </WordAsterixProvider>
    </Layout>

  )
}

export default MyApp
