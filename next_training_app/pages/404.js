import Link from 'next/link';
import { useEffect } from  'react';
import { useRouter } from 'next/router';


const FourOFour = () => {
  const router = useRouter();

  useEffect(() => {
    redirectUser();
  }, []);


  const redirectUser = () => {
    setTimeout(() => {
      router.push('/');
    }, 3000);
  };

  return (
    <div>
      <h1>Page doesn't exist Dickhead !!</h1>
      <p>You will be redirected in a few seconds to the home page.</p>
      <p>If you're not redirected you can press this link : <Link href="/"><a>Go to the Homepage</a></Link></p>
    </div>
  )
};

export default FourOFour