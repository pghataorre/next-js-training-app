import styles from '../styles/Home.module.css';
import GameInstructions from '../components/instructions';

const howtoplay = () => {
  return (
  <div className={styles.container}>
    <h1 className={styles.title}>How to play</h1>
    <GameInstructions />
  </div>
)};

export default howtoplay;