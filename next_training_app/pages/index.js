import Head from 'next/head'
import styles from '../styles/Home.module.css';
import { WordAsterixContext } from '../context/asterixContext';
import WordToGuess from '../components/wordToGuess';
import InputGuessWordForm from '../components/guessWordForm';
import Error from '../components/error';
import { getData } from '../services/service';

export default function Home(props) {
  return (
    <div className={styles.container}>
      <Head>
        <title>Next training app</title>
      </Head>
        <main className={styles.main}>
          <h1>
            GUESS THE WORD MINI QUIZ
          </h1>
          <WordAsterixContext.Consumer>
            {
              (context) => {
                const { showError } = context;
                return (
                  <>
                    { !showError 
                    ? <>
                        <WordToGuess />
                        <InputGuessWordForm />
                      </>
                    : <><Error /></>
                    }
                  </>
                )
              }
            }
          </WordAsterixContext.Consumer>
        </main>
    </div>
  )
};

 export const getStaticProps = async (context) => {
  let data = await getData();

  return {
    props: {
      secretWords: data
    }
  };
};
