const Endgame = () => {
  return (
    <>
      <h3>An error has occurred please refresh the page and try again</h3>
      <button onClick={() => location.reload() }>Try again</button>
    </>
  )
}

export default Endgame;
