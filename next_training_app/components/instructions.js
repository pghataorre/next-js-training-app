const GameInstructions = () => {
  return (
  <div>  
    <ol>
      <li>Guess the starred word that appears by inputting your guess into the input field and submitting</li>
      <li>When a letter in the starred word has been matched, it will show, giving you more of a chance to guess the correct work</li>
      <li>The game will let you guess the word an unlimited time.</li>
      <li>Once guessed you  will see a congratulation message and have the option to play again</li>
    </ol>
  </div>
)};

export default GameInstructions;