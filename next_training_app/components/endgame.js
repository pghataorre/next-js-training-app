
import React, { useContext } from 'react';
import { WordAsterixContext } from '../context/asterixContext';

const Endgame = () => {
  const { secretWord, playAgain, attempts, gameOver } = useContext(WordAsterixContext);
  const wellDoneMessage = `Correct guess: ${ secretWord }`;
  const attemptsMessage = `SORRY YOU'RE OUT OF ATTEMPTS PLAY AGAIN`;
  let message = attempts ? attemptsMessage : wellDoneMessage;
  message = gameOver ? 'CONGRATULATIONS YOU HAVE COMPLETED THE GAME' : message;
  return (
    <>
      <h2>{ message }</h2>
      { gameOver 
        ? <button onClick={ () => { location.reload() }}>Play game again</button>
        : <button onClick={ playAgain }>Guess the Next Word</button>
      }
    </>
  )
}

export default Endgame;
