import React, { useContext } from 'react';
import { setAsterisksLetters } from '../services/service';
import { WordAsterixContext } from '../context/asterixContext';
import Endgame from './endgame';
import Hint from  './hint';
import appConfig from '../config';
import styles from '../styles/Home.module.css';

const WordToGuess = () => {
  const {secretWord, guessedWord, isCorrect, attemptsCount, secretWordsCollection} = useContext(WordAsterixContext);
  const wordToGuess = setAsterisksLetters(guessedWord, secretWord);

  return (
    isCorrect || attemptsCount === appConfig.attempts
    ? 
    <Endgame />
    : <div>
        <h2 className={styles.asterisks}>{ wordToGuess }</h2>
        <Hint />
      </div>
  )
};

export default WordToGuess;
