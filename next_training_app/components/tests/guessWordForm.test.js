import "@testing-library/jest-dom/extend-expect";
import { render, fireEvent, } from "@testing-library/react";
import { WordAsterixContext } from '../../context/asterixContext';
import GuessWordForm from '../guessWordForm';

describe('Testing GuessWordForm component', () => {
  let mockStoreGuess = jest.fn();
  let mockContextValue;

  beforeEach(() => {
    mockContextValue = {
      storeGuess: mockStoreGuess,
      isCorrect: false,
      validation: false,
      attempts: false,
      attemptsCount: 2
    };
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should display the correct input field label', async () => {
    const { getByLabelText } = render(
      <WordAsterixContext.Provider value={mockContextValue}>
        <GuessWordForm />
      </WordAsterixContext.Provider>);

    const labelText = await getByLabelText('Enter your guess');
    expect(labelText).toBeInTheDocument();
  });

  it('should display an input field', async () => {
    const { container } = render(
      <WordAsterixContext.Provider value={mockContextValue}>
        <GuessWordForm />
      </WordAsterixContext.Provider>);

    const inputTextBox = await container.querySelector('#guessWord');
    expect(inputTextBox).toBeInTheDocument();
  });

  it('should NOT show a validation message as default', async () => {
    const { queryByText } = render(
      <WordAsterixContext.Provider value={mockContextValue}>
        <GuessWordForm />
      </WordAsterixContext.Provider>);

    const validationMessage = await queryByText('Please enter a guess');
    expect(validationMessage).toBe(null);
  });  

  it('should show a validation message', async () => {
    mockContextValue = {
      ...mockContextValue,
      validation: true
    };

    const { findByText } = render(
      <WordAsterixContext.Provider value={mockContextValue}>
        <GuessWordForm />
      </WordAsterixContext.Provider>);

    const validationMessage = await findByText('Please enter a guess');
    expect(validationMessage).toBeInTheDocument();
  });

  it('should show a button with correct text as default', async () => {
    mockContextValue = {
      ...mockContextValue,
      attemptsCount: 0
    };

    const { findByText } = render(
      <WordAsterixContext.Provider value={mockContextValue}>
        <GuessWordForm />
      </WordAsterixContext.Provider>);

    const buttonMessage = await findByText('Guess Word - Hit me !');
    expect(buttonMessage).toBeInTheDocument();
  });

  it('should show a button with attempted guess text', async () => {
    mockContextValue = {
      ...mockContextValue,
      isCorrect: true
    };
    const { container } = render(
      <WordAsterixContext.Provider value={mockContextValue}>
        <GuessWordForm />
      </WordAsterixContext.Provider>);

    const inputTextBox = await container.querySelector('#guessWord');
    expect(inputTextBox).toBe(null);
  });  

  it('should show a button with attempted guess text Attempt X of X', async () => {
    mockContextValue = {
      ...mockContextValue,
      attemptsCount: 4
    };
    const { findByText } = render(
      <WordAsterixContext.Provider value={mockContextValue}>
        <GuessWordForm />
      </WordAsterixContext.Provider>);

    const buttonMessage = await findByText(`Attempt 4 of 10`);
    expect(buttonMessage).toBeInTheDocument();
  });  

  it('should allow an input field to take values', async () => {
    mockContextValue = {
      ...mockContextValue,
      isCorrect: false,
      attemptsCount: 0
    };
    const { container } = render(
      <WordAsterixContext.Provider value={mockContextValue}>
        <GuessWordForm />
      </WordAsterixContext.Provider>);

    const inputTextBox = await container.querySelector('#guessWord');
    fireEvent.change(inputTextBox, { target: { value: "Test" }});

    expect(inputTextBox.value).toBe('Test');
  }); 

  it('should send the correct value when submit button clicked', async () => {
    mockContextValue = {
      ...mockContextValue,
      isCorrect: false,
      attemptsCount: 0
    };

    const { container } = render(
      <WordAsterixContext.Provider value={mockContextValue}>
        <GuessWordForm />
      </WordAsterixContext.Provider>);

    const inputTextBox = await container.querySelector('#guessWord');
    fireEvent.change(inputTextBox, { target: { value: "text" }});

    const button = await container.querySelector('#guess-word-button');
    fireEvent.click(button);

    expect(mockStoreGuess).toHaveBeenCalledTimes(1);
    expect(mockStoreGuess).toHaveBeenCalledWith("text");
  }); 
});
