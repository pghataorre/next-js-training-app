import "@testing-library/jest-dom/extend-expect";
import { render, fireEvent, } from "@testing-library/react";
import { WordAsterixContext } from '../../context/asterixContext';
import Endgame from '../endgame';

describe('Testing Endgame component', () => {
  let mockContext;;
  
  beforeEach(() => {
    mockContext = { 
      secretWord: 'Fantastic', 
      playAgain: jest.fn(), 
      attempts: false, 
      gameOver: false
    };

  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should output a correct guess message when word is guessed', async () => {    
    const { container, findByText } = render(
      <WordAsterixContext.Provider value={mockContext}>
        <Endgame/>
      </WordAsterixContext.Provider>);

    const message = await findByText(`Correct guess: ${ mockContext.secretWord }`);
    expect(message).toBeInTheDocument();
  });

  it('should output an attempts message when word hasnt not been guesed over 10 times', async () => {
    mockContext = {
      ...mockContext,
      attempts: true,
    };

    const { findByText } = render(
      <WordAsterixContext.Provider value={mockContext}>
        <Endgame />
      </WordAsterixContext.Provider>);
    const message = await findByText(`SORRY YOU'RE OUT OF ATTEMPTS PLAY AGAIN`);
    expect(message).toBeInTheDocument();

  });

  it('should output Guess the Next Wordq button', async () => {
    mockContext = {
      ...mockContext,
      attempts: false,
    };

    const { findByText } = render(
      <WordAsterixContext.Provider value={mockContext}>
        <Endgame />
      </WordAsterixContext.Provider>  
      );
      const buttonType = await findByText('Guess the Next Word')
    expect(buttonType).toBeInTheDocument();
  });

  it('should output game over message', async () => {
    mockContext = {
      ...mockContext,
      gameOver: true
    };

    const { findByText } = render(
      <WordAsterixContext.Provider value={mockContext}>
        <Endgame />
      </WordAsterixContext.Provider>);

    const message = await findByText('CONGRATULATIONS YOU HAVE COMPLETED THE GAME');
    expect(message).toBeInTheDocument();
  }); 

  it('should output Play again button', async () => {
    mockContext = {
      ...mockContext,
      gameOver: true
    };

    const { findByText } = render(
      <WordAsterixContext.Provider value={mockContext}>
        <Endgame />
      </WordAsterixContext.Provider>);

    const buttonType = await findByText('Play game again');
    expect(buttonType).toBeInTheDocument();
  });

  it('should be able to click "Guess the Next Word" button', async () => {
    const mockFunction = jest.fn();

    mockContext = {
      ...mockContext,
      playAgain: mockFunction,
      gameOver: false
    };

    const { findByText } = render(
      <WordAsterixContext.Provider value={mockContext}>
        <Endgame />
      </WordAsterixContext.Provider>);
  
    const button = await findByText('Guess the Next Word');
    await fireEvent.click(button);
    
    expect(mockFunction).toHaveBeenCalledTimes(1);
  });

  it('should be able to click "Play game again" button', async () => {
    const mockReload = jest.fn();

    Object.defineProperty(window, "location", {
      value: {
        hash: {
          endsWith: mockReload,
          includes: mockReload
        },
        assign: mockReload,
        reload: mockReload,
      },
      writable: true
    });

    mockContext = {
      ...mockContext,
      playAgain: mockReload,
      gameOver: true
    };

    const { findByText } = render(
      <WordAsterixContext.Provider value={mockContext}>
        <Endgame/>
      </WordAsterixContext.Provider>);

    const button = await findByText('Play game again');
    await fireEvent.click(button);
    
    expect(mockReload).toHaveBeenCalledTimes(1);
  });
});
