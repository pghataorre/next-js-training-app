import "@testing-library/jest-dom/extend-expect";
import { render, fireEvent } from "@testing-library/react";
import Error from '../error';

describe('Testing Error component', () => {
  it('should show an error title', async () => {
    const { findByText } = render(<Error />);
    const message = await findByText('An error has occurred please refresh the page and try again');
    expect(message).toBeInTheDocument();
  });

  it('should click a button to reload page', async () => {
    const mockReload = jest.fn(); 

    Object.defineProperty(window, "location", {
      value: {
        hash: {
          endsWith: mockReload,
          includes: mockReload
        },
        reload: mockReload,
      },
      writable: true
    });

    const { findByText } = render(<Error />);
    const button = await findByText('Try again');
    await fireEvent.click(button);

    expect(mockReload).toHaveBeenCalledTimes(1);
  });
});