import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";
import Layout from '../layout';


describe('Testing Layout component', () => {
  let mockContext;

  it('should output top navigation', async () => {
    const { findByText } = render(<Layout />);
    const message = await findByText('Home');
    expect(message).toBeInTheDocument();
    
  });

  it('should output footer', async () => {
    const { findByText } = render(<Layout />);
    const message = await findByText('Footer text here');
    expect(message).toBeInTheDocument();
    
  });

});
