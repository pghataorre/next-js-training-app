import "@testing-library/jest-dom/extend-expect";
import { render, container, findByText } from "@testing-library/react";
import { WordAsterixContext } from '../../context/asterixContext';
import Hint from '../hint';
import { testSecretWord, testSecretWordsCollection } from '../../testData/testData';

describe('Testing Hint Component', () => {
  let mockProviderValue;

  beforeEach(()=> {
    mockProviderValue = {
      secretWord: testSecretWord,
      attemptsCount: 1,
      secretWordsCollection: testSecretWordsCollection.secret,
      isCorrect: false
    };
  });

  it('should NOT show hints as default or on an attempt less than 3', async () => {
    const { container, queryByText } = render(
      <WordAsterixContext.Provider value={mockProviderValue}>
        <Hint />
      </WordAsterixContext.Provider>);

    const hintText = await queryByText('Hint :');
    expect(hintText).toBe(null);
  });

  it('should NOT show hints when the  word guess is Correct', async () => {
    mockProviderValue = {
      ...mockProviderValue,
      isCorrect: true,
    };

    const { container, queryByText } = render(
      <WordAsterixContext.Provider value={mockProviderValue}>
        <Hint />
      </WordAsterixContext.Provider>);

    const hintText = await queryByText('Hint :');
    expect(hintText).toBe(null);
  });


  it('should show first hint on Third attempt', async () => {
    mockProviderValue = {
      ...mockProviderValue,
      attemptsCount: 3
    };
    
    const { container } = render(
      <WordAsterixContext.Provider value={mockProviderValue}>
        <Hint />
      </WordAsterixContext.Provider>);

    const hintText = await container.getElementsByClassName('asterisksHint').length;
    expect(hintText).toBe(1);
  });

  it('should NOT show a hint on after the Third Attempt or before the Sixth Attempt', async () => {
    mockProviderValue = {
      ...mockProviderValue,
      attemptsCount: 5
    };
    
    const { container } = render(
      <WordAsterixContext.Provider value={mockProviderValue}>
        <Hint />
      </WordAsterixContext.Provider>);

    const hintText = await container.getElementsByClassName('asterisksHint').length;
    expect(hintText).toBe(0);
  });

  it('should show a hint on the Sixth Attempt', async () => {
    mockProviderValue = {
      ...mockProviderValue,
      attemptsCount: 6
    };
    
    const { container } = render(
      <WordAsterixContext.Provider value={mockProviderValue}>
        <Hint />
      </WordAsterixContext.Provider>);

    const hintText = await container.getElementsByClassName('asterisksHint').length;
    expect(hintText).toBe(1);
  });

  it('should NOT show a hint after the Sixth Attempt', async () => {
    mockProviderValue = {
      ...mockProviderValue,
      attemptsCount: 8
    };
    
    const { container } = render(
      <WordAsterixContext.Provider value={mockProviderValue}>
        <Hint />
      </WordAsterixContext.Provider>);

    const hintText = await container.getElementsByClassName('asterisksHint').length;
    expect(hintText).toBe(0);
  });

});
