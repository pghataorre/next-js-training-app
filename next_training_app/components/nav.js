import Link from 'next/link';
import Image from 'next/image';

const Nav = () => {
  return (
    <nav>
      <div>
        <Image src="/desk-lamp.svg" width={60} height={60} />
      </div>
      <ul>
        <li><Link href="/"><a>Home</a></Link></li>
        <li><Link href="/howtoplay"><a>How to play</a></Link></li>
      </ul>
    </nav>
  )
};

export default Nav;
