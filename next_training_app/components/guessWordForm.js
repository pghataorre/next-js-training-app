import React, { useContext, useState } from "react";
import { WordAsterixContext } from "../context/asterixContext";

const InputGuessWord = () => {
  const {
    storeGuess,
    isCorrect,
    validation,
    attempts,
    attemptsCount,
  } = useContext(WordAsterixContext);
  let showForm = false;

  const [guessWord, setGuessWord] = useState("");

  if (!isCorrect) {
    if (attempts) {
      showForm = true;
    }
  } else {
    showForm = true;
  }

  return (
    <div>
      {!showForm && (
        <>
          <label htmlFor="guessWord">Enter your guess</label>
          <input
            type="text"
            id="guessWord"
            name="guessWord"
            value={guessWord}
            onChange={(event) => setGuessWord(event.target.value)}
          />
          <button
            type="submit"
            id="guess-word-button"
            onClick={() => storeGuess(guessWord)
            }
          >
            Guess Word{" "}
            {attemptsCount > 0 && <span>Attempt {attemptsCount} of 10</span>} -
            Hit me !
          </button>
          {validation && (
            <p className="validtaion-error">Please enter a guess</p>
          )}
        </>
      )}
    </div>
  );
};

export default InputGuessWord;
