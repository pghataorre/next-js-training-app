import React, { useContext } from 'react';
import { WordAsterixContext } from '../context/asterixContext';
import { getSecretWordHints } from '../services/service';
import styles from '../styles/Home.module.css';

const Hint = () => {
  const {secretWord, attemptsCount, secretWordsCollection, isCorrect} = useContext(WordAsterixContext);
  let secretWordHint = '';
  let showHint  = false;

  if (secretWord !== '' && !isCorrect) {
    secretWordHint = getSecretWordHints(secretWord, secretWordsCollection);
  }

  if (attemptsCount === 3 && !isCorrect) {
    showHint = true;
    secretWordHint = secretWordHint[0];
  } else if(attemptsCount === 6 & !isCorrect) {
    showHint = true;
    secretWordHint = secretWordHint[1];
  }

  return (showHint ? (<p className={styles.asterisksHint}>Hint: { secretWordHint }</p>) : '');
};

export default Hint;
