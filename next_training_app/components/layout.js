import Nav from './nav';

const Layout = ({ children }) => {
  
  return (
    <>
      <Nav />
      { children }
      <footer>Footer text here</footer>
    </>
  )
};

export default Layout;