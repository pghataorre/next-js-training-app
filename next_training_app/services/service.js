import appConfig from '../config';

const getLetterMatchCount = (guessedWord, secretWord) => {
  const secretLetterSet = new Set(secretWord.split(''));
  const guessedLetterSet = new Set(guessedWord.split(''));

  if (guessedWord === '') {
    return 0;

  } else {
    return [...secretLetterSet].filter((letterItem) => {
       return guessedLetterSet.has(letterItem);
    }).length;
  }
};

const setAsterisksLetters = (guessedWord, secretWord) => {
  if (!guessedWord && !secretWord) return;

  const secretWordLettersArray = secretWord.split('');
  let fullWord = '';
  if (!guessedWord) return convertToAsterisks(secretWordLettersArray);

  const guessedWordLettersArray =  guessedWord.split('');
  const matchingLetters = guessedWordLettersArray.filter((item) => {
    return secretWordLettersArray.includes(item.toString()) ? item : null;
  });

  secretWordLettersArray.map((item) => {
    let i = 0;
    let letter = '';
    let matchingLettersLength = matchingLetters.length;

    for(i; i < matchingLettersLength; i++) {
      if (item === matchingLetters[i]) {
        letter = item;
        break;
      }
    };

    fullWord += letter === '' ? '*' : letter;
  });

  return fullWord;
};

const convertToAsterisks = (secretWord) => {
  const secretWordArray = typeof secretWord === "string" ? secretWord.split('') :  secretWord;
  const asterisks = '*';
  return secretWordArray.map(() => {
    return asterisks;
  }).join('');
};

const getData = async () => {
  try {
    const response = await fetch(appConfig.api);
    const data = await response.json();
    return data;
  } catch(error) {
    return {error: {message: error.message, code: error.message}};
  }
};

const getSecretWordsFromObject = (secretWordsCollection) => {
  return secretWordsCollection.map((item) => {
    return item;
  });
};

const getSecretWordHints = (secretWord, secretWordCollection) => {
  if(!secretWord) return;
  
  let i = 0;
  let hintArray;

  for(i; i<secretWordCollection.length; i++) {
    if (secretWordCollection[i].word === secretWord) {
      hintArray = secretWordCollection[i].wordHints;
      break;
    } 
  }

  return hintArray;
};

export {
  getLetterMatchCount,
  setAsterisksLetters,
  convertToAsterisks,
  getData,
  getSecretWordsFromObject,
  getSecretWordHints
};
