import {
  getLetterMatchCount,
  setAsterisksLetters,
  convertToAsterisks,
  getData,
  getSecretWordsFromObject,
  getSecretWordHints  
} from './service';
import { 
  testSecretWordsObject,
  testSecretWordsCollection, 
  testSecretWord,
  testSecretWordHints 
} from '../testData/testData';

describe('Testing Service Functions', () => {

  describe('Testing getLetterMatchCount', () => {
    it('should handle no letters passed in', () => {
      const testLetterCount =  getLetterMatchCount('', testSecretWord);
      expect(testLetterCount).toBe(0);
    });

    it('should handle letters passed in that are not contained in the test Secret word', () => {
      const testLetterCount =  getLetterMatchCount('qz', testSecretWord);
      expect(testLetterCount).toBe(0);
    });

    it('should return a count of the letters that are passed in and are contained in the test Secret word', () => {
      const testLetterCount =  getLetterMatchCount('an', testSecretWord);
      expect(testLetterCount).toBe(2);
    });
  });

  describe('Testing setAsterisksLetters', () => {
    it('should return all Asterisks instead of works when no guess is passed in', () => {
      const testLetterCount =  setAsterisksLetters('', testSecretWord);
      expect(testLetterCount).toBe('******');
    });

    it('should return letters and Asterisks when matching letter are passed in', () => {
      const testLetterCount =  setAsterisksLetters('O', testSecretWord);
      expect(testLetterCount).toBe('O*****');
    });

    it('should return letters when the word is matched', () => {
      const testLetterCount =  setAsterisksLetters(testSecretWord, testSecretWord);
      expect(testLetterCount).toBe(testSecretWord);
    });
  });

  describe('Testing convertToAsterisks', () => {
    it('should convert the whole word to Asterisks', () => {
        const testWord = 'Fantastic';
        const testAsterisks = convertToAsterisks(testWord);
        expect(testAsterisks).toBe('*********');
    });
  });

  describe('Testing getData', () => {
    const testApiResponse = testSecretWordsCollection.secret;
    const testErrorResponse = {
      "error": {
        "code": undefined,
        "message": undefined,
      }
    };

    const mockFetch = Promise.resolve({ json: () => Promise.resolve(testApiResponse) });
    global.fetch = jest.fn().mockImplementation(() => mockFetch);

    beforeEach(() => {
      fetch.mockClear();
    });

    it('should call fetch', async () => {
      const testData = await getData();
      expect(fetch).toHaveBeenCalledTimes(1);
    });

    it('should return correct API response', async () => {
      const data = await getData();
      expect(data).toEqual(testApiResponse);
    });

    it('should return a Failed Response', async () => {
      fetch.mockImplementation(() => Promise.reject(testErrorResponse));
      const data = await getData();
      expect(data).toEqual(testErrorResponse);
    });
  });

  describe('Testing getSecretWordsFromObject', () => {
    it('should return an array of words', () => {
      const secretWordsArrayResult = getSecretWordsFromObject(testSecretWordsCollection.secret);
      expect(secretWordsArrayResult).toEqual(testSecretWordsObject);
    });
  });

  describe('Testing getSecretWordHints', () => {
    it('should return an array with Hints', () => {
      const secretWordsHintsArray = getSecretWordHints(testSecretWord, testSecretWordsCollection.secret);
      expect(secretWordsHintsArray).toEqual(["A fruit", "a Citric fruit"]);
    });
  });

});
