## Getting Started and installing a Default Next.js project

Open a command line and find a directory that you wish install your project. 

Run the following command.
$ npm install NPX 

When completed run the following 
$ npx create-next-app

Then the installer will ask what you want to call your project (this-is-allowed-for-project-name) - (this is NOT allowed )  - NO SPACES

$ cd NAME_OF_YOUR_PROJECT

(Running the project)
$ npm run dev

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

### ------------------------------------------------
## Opening this particular project

This App is a simple word guessing game, it requires the installation of two repositories



### The Backend services app that the app needs Clone repo:
$ git clone git@bitbucket.org:pghataorre/expres-micro-service.git

Find the directory expres-micro-service and get the root of folder 
(keep going into the folders until you find a package.json file)
$ cd expres-micro-service

(Install all the dependencies fo the project)
$ npm install

(Running the project)
$ npm run start

The app runs locally on port 3333.
Open [http://localhost:3333](http://localhost:3333) with your browser to view the app.

### The front end app Clone this repo:
$ git clone git@bitbucket.org:pghataorre/next-js-training-app.git

Find the directory next_training_app and get the root of folder 
(keep going into the folders until you find a package.json file)
$ cd next_training_app

(Install all the dependencies fo the project)
$ npm install

(Running the project)
$ npm run dev

The app runs locally on port 3001.
Open [http://localhost:3001](http://localhost:3001) with your browser to view the app.
### ------------------------------------------------

## Routing

Extremely simple in NextJs just within the pages folder create the name of your page
e.g

testPage.js maps to localhost:3000/testPage
someDirectory/testPage.js maps to localhost:3000/someDirectory/testPage

### ------------------------------------------------
## Creating Links

Just like using Link within React expect its baked in and no other install is required

Need to import the Link Component
import Link from 'next/link';

Then simply wrap the <a> link tag with link <Link>

<Link href="FILE_NAME_OF_THE_PAGE_TO_LINK_TO"><a>SOME LINK TITLE</a></Link>

### ------------------------------------------------
## Using Next Routers

Routers allow you to redirect pages e.g. say when a form is submitted, or when you want to redirect a user after little time on a page.

Use next/router

import { useRouter }  from 'next/router';

const router = useRouter();

router.push('/SOME_INTENDED_URL');

## ------------------------------------------------
## Using Next Image

You can use the standard <img> tag from html, but theres a few more benefits of using NextJs Image
Allows more strict use as you MUST apply height and width
It allows baked in auto lazy loading.
Used like a normal component.

import Image from 'next/image';

<Image src="/PATH_TO_IMAGE_USUALLY_IN_public_FOLDER" width={100} height={100} />

## ------------------------------------------------

## Data Fetching - YOUR_CHOSEN.COMPONENT.getInitialProps

Next js can fetch data server side and using .getInitialProps this can be done.

```javascript
// Your component as usual
// PARAMETERS stars  -- This is actually the results from the api call.
const YOUR_COMPONENT = ({ stars }) => {  
  return <div>Next stars: {stars}</div>
}

YOUR_COMPONENT.getInitialProps = async () => {
  const res = await fetch('https://SOME_API')
  const json = await res.json()
  return { stars: json.stargazers_count } // stars gets returned to the components as (React props)
}

export default YOUR_COMPONENT
```

## Additional
[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## ------------------------------------------------

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.

## ------------------------------------------------
